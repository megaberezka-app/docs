<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>API Reference</title>
  <link rel="stylesheet" href="https://stackedit.io/style.css" />
</head>

<body class="stackedit">
  <div class="stackedit__left">
    <div class="stackedit__toc">
      
<ul>
<li><a href="#описание-api-взаимодействия-мобильного-приложения-с-серверной-стороной">Описание API взаимодействия мобильного приложения с серверной стороной</a>
<ul>
<li><a href="#общее-описание">Общее описание</a></li>
<li><a href="#список-конечных-точек-endpoints">Список конечных точек (endpoints)</a></li>
</ul>
</li>
</ul>

    </div>
  </div>
  <div class="stackedit__right">
    <div class="stackedit__html">
      <h1 id="описание-api-взаимодействия-мобильного-приложения-с-серверной-стороной">Описание API взаимодействия мобильного приложения с серверной стороной</h1>
<h2 id="общее-описание">Общее описание</h2>
<p>В основе RESTful парадигма.<br>
Конечные точки строятся на основе директорий.<br>
Корень сервера /api/{version}/{endpoint}</p>
<p>Запрос от клиента - Request. Это json структура. Запросы могут быть как авторизованные, для этого в HTTP Header должна быть запись вида:<br>
<code>Authorization: Bearer {JWT}</code><br>
где JWT - корректный токен, сформированный в результате успешного логина (подробнее здесь: <a href="https://jwt.io/">https://jwt.io/</a>)</p>
<p>Ответ на запрос клиента - Response. Так же json структура, возвращающая HTTP Status Code результата (подробнее: <a href="https://en.wikipedia.org/wiki/List_of_HTTP_status_codes">https://en.wikipedia.org/wiki/List_of_HTTP_status_codes</a>) и произвольные данные.</p>
<h2 id="список-конечных-точек-endpoints">Список конечных точек (endpoints)</h2>
<h3 id="login">login</h3>
<p>Используется для авторизации пользователя в системе</p>
<h4 id="request">Request</h4>

<table>
<thead>
<tr>
<th>#</th>
<th>Параметр</th>
<th>Тип</th>
<th>Обязательно</th>
<th>Уникально</th>
<th>Описание</th>
</tr>
</thead>
<tbody>
<tr>
<td>1.</td>
<td>login</td>
<td>string</td>
<td>*</td>
<td></td>
<td>Идентификатор пользователя в системе</td>
</tr>
<tr>
<td>2.</td>
<td>password</td>
<td>string</td>
<td>*</td>
<td></td>
<td>Пароль пользователя</td>
</tr>
</tbody>
</table><h4 id="response">Response</h4>

<table>
<thead>
<tr>
<th>#</th>
<th>Параметр</th>
<th>Тип</th>
<th>Обязательно</th>
<th>Уникально</th>
<th>Описание</th>
</tr>
</thead>
<tbody>
<tr>
<td>1.</td>
<td>login</td>
<td>string</td>
<td>*</td>
<td></td>
<td>Идентификатор пользователя в системе</td>
</tr>
<tr>
<td>2.</td>
<td>password</td>
<td>string</td>
<td>*</td>
<td></td>
<td>Пароль пользователя</td>
</tr>
</tbody>
</table>
    </div>
  </div>
</body>

</html>
